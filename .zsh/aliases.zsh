# My aliases
alias c="clear"
alias b="cd -"
alias h="cd ~"
alias x="logout"
alias p="cd ~/projects/"

alias gs="git status"
alias gc="git commit"
alias gcm="git add -A . && git commit -am"
alias gr="git pull --rebase"
alias gsafe="git push --force-with-lease"

alias g="git"

function gitdays {
  git log --author=laribee --reverse --since="$@ days ago" --pretty="format:%n%Cgreen%cd%n%n%s%n%b%n---------------------------------------------"
}